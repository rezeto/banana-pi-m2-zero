<h1>Arch Linux for Banana Pi M2 Zero </h1>

<h2> How to prepare memory card for Banana Pi </h2>
1. Prepare SDCARD:

- Downloads ArchLinuxArm from mirror 	ArchLinuxARM-armv7-latest.tar.gz (example https://uk.mirror.archlinuxarm.org/os/)
- Create bootable partition ( from cli or with gparted )

- bootloader ( u-boot )
  * Download from git clone https://github.com/u-boot/u-boot
  * Enter in the downloaded directory with cd and execute:<br>
    make CROSS_COMPILE=arm-linux-gnueabihf- bananapi_m2_zero_defconfig <br>
    make menuconfig ( optional ) <br>
    make CROSS_COMPILE=arm-linux-gnueabihf- <br>
  * Write u-boot to sdcard:
  sudo dd if=u-boot-sunxi-with-spl.bin of=/dev/mmcblk0 bs=1024 seek=8 status=progress

- boot.cmd ( Legacy )
  * Use script from https://github.com/TuryRx/Banana-pi-m2-zero-Arch-Linux#banana-pi-m2-zero-arch-linux-
- extlinux

  boot/extlinux/extlinux.conf <br>
  label ArchLinuxArm <br>
        menu label Arch Linux ARM <br>
        kernel /boot/zImage <br>
        fdtdir /boot/dtbs <br>
        append initrd=/boot/initramfs-linux.img root=UUID=2a46d51c-e118-426b-80d4-55bed678d744 rw quiet <br>


2. Configure WiFi:

- Download packages which are not included in the image but are needed for wifi to work( https://uk.mirror.archlinuxarm.org/armv7h/ )
  * duktape-2.7.0-6-armv7h.pkg
  * polkit-123-1-armv7h.pkg
  * pcsclite-2.0.0-1-armv7h.pkg
  * wpa_supplicant-2 2.10-8-armv7h.pkg

- install the packages ( offline in the order provided )
  * pacman -U duktape-2.7.0-6-armv7h.pkg.tar.xz
  * pacman -U polkit-123-1-armv7h.pkg.tar.xz
  * pacman -U pcsclite-2.0.0-1-armv7h.pkg.tar.xz
  * pacman -U wpa_supplicant-2\ 2.10-8-armv7h.pkg.tar.xz

- Configure netctl:

  * Create file /etc/netctl/home <br>
  """ <br>
  #Interface=wlan0  <br>
  #Connection=wireless <br>
  #Security=wpa <br>
  #ESSID=$WIFI_NAME <br>
  #IP=dhcp <br>
  #Key=$PASSWORD <br>
  """ <br>

  * netctl list
  * netctl start home
  * dchpcd -d
  * netctl enable home

If you decide to use wifi-menu, it's needed to install one more package:
  * dialog-1 1.3_20230209-1-armv7h.pkg




3. To activate pacman execute following commands:
  * pacman-key --init
  * pacman-key --populate archlinuxarm


4. For enabling the bluetooth
- Install git package:
  * pacman -S make git
- Clone git repo:
  * git clone https://github.com/RPi-Distro/bluez-firmware/tree/master
- build and install the bluez-firmware:
  * ./configure --libdir=/lib
  * make && make install
- Start and enable systemd service:
  * systemctl start bluetooth.service
  * systemctl enable bluetooth.service

- Use bluetoothctl to pair with some device.

<h2>Use the preconfigured image </h2>
1. Download the image from - https://mega.nz/folder/shcAHTQS#zy2BnLH-aaOVxDQFwAqmQg <br>
2. Decompress image and write to memory card with dd. <br>
3. Edit the config file /etc/netctl/home, change $WIFI_NAME and $PASSWORD with your wifi network and password. <br>


<h2>What work HDMI ( CLI only, after successfull boot ), GPIO , WIFI, Bluetooth</h2>
